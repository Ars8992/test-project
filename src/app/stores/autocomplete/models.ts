import { LngLatLike } from 'maplibre-gl';
import { AutocompleteGroupResponse } from '../../apis/autocomplete/models';

export interface AutocompleteStoreState extends AutocompleteGroupResponse {
  selectedPlaceCoordinates: LngLatLike;
}

