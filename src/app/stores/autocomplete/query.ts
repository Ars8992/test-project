import { Injectable } from '@angular/core';
import { Query } from '@datorama/akita';
import { GeoJSON } from 'geojson';
import { LngLatLike } from 'maplibre-gl';
import { Observable } from 'rxjs';
import { AutocompleteStoreState } from './models';
import { AutocompleteResponseStore } from './store';

@Injectable({ providedIn: 'root' })
export class AutocompleteQuery extends Query<AutocompleteStoreState> {
  constructor(store: AutocompleteResponseStore) {
    super(store);
  }

  public selectAutocompleteFeatures(): Observable<GeoJSON.Feature[]> {
    return this.select('features');
  }

  public selectCoordinates(): Observable<LngLatLike> {
    return this.select('selectedPlaceCoordinates');
  }
}
