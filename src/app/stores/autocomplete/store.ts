import { Injectable } from '@angular/core';
import { Store, StoreConfig } from '@datorama/akita';
import { LngLatLike } from 'maplibre-gl';
import { AutocompleteStoreState } from './models';

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'autocomplete', resettable: true })
export class AutocompleteResponseStore extends Store<AutocompleteStoreState> {
  constructor() {
    super(createInitialState());
  }
}

export function createInitialState() {
  return {
    features: [],
    selectedPlaceCoordinates: [30.316229, 59.938732] as LngLatLike,
  }
}
