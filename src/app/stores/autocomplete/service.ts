import { Injectable } from '@angular/core';
import { LngLatLike } from 'maplibre-gl';
import { mapTo, Observable, tap } from 'rxjs';
import { AutocompleteRequest } from '../../apis/autocomplete/models';
import { AutocompleteApiService } from '../../apis/autocomplete/service';
import { AutocompleteResponseStore } from './store';

@Injectable({ providedIn: 'root' })
export class AutocompleteService {
  constructor(
    private readonly api: AutocompleteApiService,
    private store: AutocompleteResponseStore,
  ) {
  }

  public loadAutocompleteGroups(model: Partial<AutocompleteRequest>): Observable<void> {
    return this.api.getAutocompleteGroup(model).pipe(
      tap(response => {
        this.store.setLoading(true);
        this.store.update(() => ({
          ...response,
        }))
      }),
      mapTo(void 0),
    )
  }

  public setSelectedCoordinates(coordinates: LngLatLike): void {
    this.store.update(() => ({
      features: [],
      selectedPlaceCoordinates: coordinates,
    }))
  }

  public clearAutocompleteGroups(): void {
    this.store.update(() => ({
      features: [],
    }))
  }
}
