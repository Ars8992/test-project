import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { NgxMapLibreGLModule } from 'ngx-maplibre-gl';
import { AutocompleteElementComponent } from './components/autocomplete-element/autocomplete-element.component';
import { MapComponent } from './components/map/map.component';
import { SearchComponent } from './components/search/search.component';

@NgModule({
  declarations: [
    SearchComponent,
    MapComponent,
    AutocompleteElementComponent,
  ],
  exports: [
    SearchComponent,
    MapComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    NgxMapLibreGLModule,
  ],
})
export class AutocompleteModule {}
