import { Injectable } from '@angular/core';
import { LngLatLike } from 'maplibre-gl';
import { Observable } from 'rxjs';
import { AutocompleteRequest } from '../../apis/autocomplete/models';
import { AutocompleteService } from '../../stores/autocomplete/service';

@Injectable()
export class AutocompleteUseCases {
  constructor(
    private readonly autocompleteService: AutocompleteService,
  ) {
  }

  public loadAutocompleteGroups(model: Partial<AutocompleteRequest>): Observable<void> {
    return this.autocompleteService.loadAutocompleteGroups(model);
  }

  public setSelectedCoordinates(coordinates: LngLatLike): void {
    this.autocompleteService.setSelectedCoordinates(coordinates);
  }

  public clearAutocompleteGroups(): void {
    this.autocompleteService.clearAutocompleteGroups();
  }
}
