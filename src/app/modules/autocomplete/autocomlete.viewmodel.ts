import { Injectable } from '@angular/core';
import { GeoJSON } from 'geojson';
import { LngLatLike } from 'maplibre-gl';
import { Observable } from 'rxjs';
import { AutocompleteQuery } from '../../stores/autocomplete/query';

@Injectable({ providedIn: 'root' })
export class AutocompleteViewModel {
  constructor(
    private readonly autocompleteQuery: AutocompleteQuery,
  ) {
  }

  public selectAutocompleteFeatures(): Observable<GeoJSON.Feature[]> {
    return this.autocompleteQuery.selectAutocompleteFeatures();
  }

  public selectCoordinates(): Observable<LngLatLike> {
    return this.autocompleteQuery.selectCoordinates();
  }
}
