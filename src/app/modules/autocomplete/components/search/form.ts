import { Injectable } from '@angular/core';
import { FormBuilder } from '@angular/forms';

@Injectable()
export class AutocompleteSearchForm {
  public readonly formGroup = this.fb.group({
    searchInput: this.fb.control(''),
  })

  constructor(
    private fb: FormBuilder,
  ) {
  }
}
