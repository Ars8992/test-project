import { Component, OnInit } from '@angular/core';
import { debounce, filter, map, tap, timer } from 'rxjs';
import { environment } from '../../../../../environments/environment';
import { AutocompleteViewModel } from '../../autocomlete.viewmodel';
import { AutocompleteUseCases } from '../../autocomplete.usecases';
import { AutocompleteElementComponent } from '../autocomplete-element/autocomplete-element.component';
import { AutocompleteSearchForm } from './form';

@Component({
  selector: 'search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css'],
  providers: [AutocompleteUseCases, AutocompleteSearchForm],
})
export class SearchComponent implements OnInit {
  public readonly geoFeatures = this.viewModel.selectAutocompleteFeatures();

  constructor(
    public readonly form: AutocompleteSearchForm,
    private readonly useCases: AutocompleteUseCases,
    private readonly viewModel: AutocompleteViewModel,
  ) {
  }

  public ngOnInit(): void {
    this.form.formGroup.get('searchInput')?.valueChanges.pipe(
      map(value => {
        if (!value) {
          this.useCases.clearAutocompleteGroups();
        }

        return value;
      }),
      filter(value => !!value),
      debounce(() => timer(1000)),
      tap(value => {
        this.useCases.loadAutocompleteGroups({text: value, apiKey: environment.geoApiKey}).subscribe()
      }),
    ).subscribe();
  }

  public setSelectedPlace(selectedAddress: AutocompleteElementComponent.SelectedPlace): void {
    this.useCases.setSelectedCoordinates(selectedAddress.coordinates);
    this.form.formGroup.setValue({ searchInput: selectedAddress.fullAddress }, { emitEvent: false });
  }
}
