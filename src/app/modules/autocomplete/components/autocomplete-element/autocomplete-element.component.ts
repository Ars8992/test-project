import { Component, EventEmitter, HostListener, Input, Output } from '@angular/core';
import { GeoJSON, Point } from 'geojson';
import { LngLatLike } from 'maplibre-gl';

@Component({
  selector: 'autocomplete-element',
  templateUrl: './autocomplete-element.component.html',
  styleUrls: ['./autocomplete-element.component.css'],
})
export class AutocompleteElementComponent {
  @Input()
  public feature!: GeoJSON.Feature;

  @Output()
  public selectedPlace = new EventEmitter<AutocompleteElementComponent.SelectedPlace>();

  @HostListener('click')
  public onClick(): void {
    this.selectedPlace.emit({
      coordinates: (this.feature.geometry as Point).coordinates as LngLatLike,
      fullAddress: `${this.feature.properties!['address_line1']}, ${this.feature.properties!['address_line2']}`,
    });
  }
}

export namespace AutocompleteElementComponent {
  export interface SelectedPlace {
    coordinates: LngLatLike;
    fullAddress: string;
  }
}
