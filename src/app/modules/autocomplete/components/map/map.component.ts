import { Component } from '@angular/core';
import { MAP_STYLE_URL, ZOOM_INITIAL_VALUE } from '../../../../constants';
import { AutocompleteViewModel } from '../../autocomlete.viewmodel';

@Component({
  selector: 'map-component',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent {
  public MAP_STYLE_URL = MAP_STYLE_URL;
  public ZOOM_INITIAL_VALUE = ZOOM_INITIAL_VALUE;
  public coordinates = this.viewModel.selectCoordinates();

  constructor(
    private readonly viewModel: AutocompleteViewModel,
  ) {
  }
}
