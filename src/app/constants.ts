import { environment } from '../environments/environment';

export const BASE_API_URL = 'https://api.geoapify.com/v1/geocode';
export const MAP_STYLE_URL = `https://maps.geoapify.com/v1/styles/osm-carto/style.json?apiKey=${environment.geoApiKey}`;
export const ZOOM_INITIAL_VALUE = 13;
