import { GeoJSON } from 'geojson';

export interface AutocompleteGroupResponse extends GeoJSON.FeatureCollection {
}

export interface AutocompleteRequest {
  apiKey: string;
  text: string;
  type: AutocompleteTypes;
  lang: string;
  filter: string;
  bias: string;
  format: AutocompleteResponseFormat;
}

export type AutocompleteTypes = 'country' | 'state' | 'city' | 'postcode' | 'street' | 'amenity'
export type AutocompleteResponseFormat =  'json' | 'xml' | 'geojson'
