import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BASE_API_URL } from '../../constants';
import { AutocompleteGroupResponse, AutocompleteRequest } from './models';

@Injectable({ providedIn: 'root' })
export class AutocompleteApiService {
  constructor(
    private readonly httpClient: HttpClient,
  ) {
  }

  public getAutocompleteGroup(requestParams: Partial<AutocompleteRequest>): Observable<AutocompleteGroupResponse> {
    const params = new HttpParams().appendAll(requestParams);

    return this.httpClient.get<AutocompleteGroupResponse>(`${BASE_API_URL}/autocomplete`, { params })
  }
}
